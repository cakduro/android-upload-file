package gvmnetworks.uploadfile;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.imageView)
    ImageView imageView;

    private static final int ASK_MULTIPLE_PERMISSION = 1111;
    private static final int REQUEST_FROM_CAMERA = 1001;
    private static final int REQUEST_FROM_ALBUM = 1002;

    private String imagePath;

    private Context getContext() {
        return MainActivity.this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    void onButtonClick() {
        if (Utils.checkSupportCamera(this)) {
            onAskPermission();
        } else {
            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        }
    }

    private void onAskPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                + ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ASK_MULTIPLE_PERMISSION);
        } else {
            onPermissionGranted();
        }
    }

    private void onPermissionGranted() {
        AlertDialog.Builder takebgCoverDialog = new AlertDialog.Builder(this);
        takebgCoverDialog.setTitle("Ambil Foto");
        takebgCoverDialog.setItems(new CharSequence[]{"Kamera", "Album"}, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position) {
                switch (position) {
                    case 0:
                        try {
                            FileUtil.getFromCamera(getContext());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case 1:
                        FileUtil.getFromAlbum(getContext());
                        break;
                }
            }
        });
        takebgCoverDialog.show();
    }

    private void attachToImageView(String imagePath) {
        Glide.with(getContext())
                .load(imagePath)
                .dontAnimate()
                .into(imageView);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ASK_MULTIPLE_PERMISSION && grantResults.length > 0) {
            boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
            boolean writeExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            if (cameraPermission && writeExternalFile) {
                onPermissionGranted();
            } else {
                System.out.println("permission not granted.");
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_FROM_CAMERA) {
            if (resultCode == RESULT_OK) {
                imagePath = FileUtil.compressImage(this, null, 640);
                attachToImageView(imagePath);
            }
        } else if (requestCode == REQUEST_FROM_ALBUM) {
            if (resultCode == RESULT_OK) {
                Uri imageUri = data.getData();
                boolean isFromGoogleDrive = imageUri.toString().contains("com.google.android.apps.docs.storage");
                imagePath = FileUtil.compressImage(this, isFromGoogleDrive ?
                        FileUtil.downloadFromGoogleDrive(this, imageUri) :
                        FileUtil.getPath(this, imageUri), 640);
                attachToImageView(imagePath);
            }
        }
    }

}
